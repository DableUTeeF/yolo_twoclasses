#! /usr/bin/env python
from __future__ import print_function, division
import os

os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ["KERAS_BACKEND"] = "tensorflow"
import numpy as np
from preprocessing import parse_annotation
from frontend import YOLO
import json

if __name__ == '__main__':
    config_path = 'config/config_4.json'
    with open(config_path) as config_buffer:
        config = json.loads(config_buffer.read())

    ###############################
    #   Parse the annotations
    ###############################

    # parse annotations of the training set
    train_imgs, [train_types, train_brand] = parse_annotation(config['train']['train_annot_folder'],
                                                              config['train']['train_image_folder'],
                                                              config['model']['types'])

    # parse annotations of the validation set, if any, otherwise split the training set
    if os.path.exists(config['valid']['valid_annot_folder']):
        valid_imgs, [valid_types, valid_brand] = parse_annotation(config['valid']['valid_annot_folder'],
                                                                  config['valid']['valid_image_folder'],
                                                                  config['model']['types'])
    else:
        train_valid_split = int(0.8 * len(train_imgs))
        np.random.shuffle(train_imgs)

        valid_imgs = train_imgs[train_valid_split:]
        train_imgs = train_imgs[:train_valid_split]

    overlap_labels = set(config['model']['brand']).intersection(set(train_brand.keys()))

    print('Seen labels:\t', train_brand)
    print('Given labels:\t', config['model']['brand'])
    print('Overlap labels:\t', overlap_labels)

    if len(overlap_labels) < len(config['model']['brand']):
        raise ValueError('Some labels have no images! Please revise the list of labels in the config.json file!')

    ###############################
    #   Construct the model
    ###############################
    yolo = YOLO(architecture=config['model']['architecture'],
                input_size=config['model']['input_size'],
                types=config['model']['types'],
                brand=config['model']['brand'],
                max_box_per_image=config['model']['max_box_per_image'],
                anchors=config['model']['anchors'], trainable=True,
                debug=config['train']['debug'])

    ###############################
    #   Load the pretrained weights (if any)
    ###############################

    if os.path.exists(config['train']['pretrained_weights']):
        print("Loading pre-trained weights in", config['train']['pretrained_weights'])
        yolo.load_weights(config['train']['pretrained_weights'])  # ########################

    ###############################
    #   Start the training process
    ###############################
    # yolo.model.load_weights(config['train']['pretrained_weights'])
    yolo.train(train_imgs=train_imgs,
               valid_imgs=valid_imgs,
               train_times=config['train']['train_times'],
               valid_times=config['valid']['valid_times'],
               nb_epoch=config['train']['nb_epoch'],
               learning_rate=config['train']['learning_rate'],
               batch_size=config['train']['batch_size'],
               warmup_epochs=config['train']['warmup_epochs'],
               object_scale=config['train']['object_scale'],
               no_object_scale=config['train']['no_object_scale'],
               coord_scale=config['train']['coord_scale'],
               class_scale=config['train']['class_scale'],
               saved_weights_name=config['train']['saved_weights_name'],
               debug=config['train']['debug'])
