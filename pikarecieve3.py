
from __future__ import print_function
import numpy as np
import os
from frontend import YOLO
import json
from utils import return_label
import pika
import base64

import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
# todo: Try separate odd and even ID and check if it faster.
config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.25
set_session(tf.Session(config=config))


config_path = 'config/config_4.json'
models = 'yolo'
with open(config_path) as config_buffer:
    config = json.loads(config_buffer.read())

yolo = YOLO(architecture=config['model']['architecture'],
            input_size=config['model']['input_size'],
            types=config['model']['types'],
            brand=config['model']['brand'],
            max_box_per_image=config['model']['max_box_per_image'],
            anchors=config['model']['anchors'], trainable=True,
            debug=config['train']['debug'])
if os.path.exists(config['train']['pretrained_weights']):
    print("Loading pre-trained weights in", config['train']['pretrained_weights'])
    yolo.load_weights(config['train']['pretrained_weights'])


def _main_(image, id, loc):

    global yolo

    boxes, out = yolo.predict(image, confidence_threshold=0.0, confidence_comfirm=False)
    image = return_label(image, boxes, config['model']['types'], config['model']['brand'], id, loc)
    return image


def callback(_, __, ___, body):
    try:
        loc, ids, imgarray = body.split(b' ')  # sender location, id, image
        imgarray = np.frombuffer(base64.b64decode(imgarray), np.uint8)
        imgarray = np.reshape(imgarray, (284, 160, 3))
        imgarray = imgarray[:, :, ::-1]
        img = _main_(imgarray, ids, loc)
        print("sending output")
        r_url = os.environ.get('CLOUDAMQP_URL', 'amqp://admin:Password...@10.226.49.117/%2f')
        r_params = pika.URLParameters(r_url)
        r_connection = pika.BlockingConnection(r_params)
        r_channel = r_connection.channel()
        r_channel.queue_declare(queue='output_send3')
        r_channel.basic_publish(exchange='',
                                routing_key='output_send3',
                                body=str(img))
        print(f"output {str(img)} sent")
        r_connection.close()
    except ValueError as e:
        print(e)


if __name__ == '__main__':
    url = os.environ.get('CLOUDAMQP_URL', 'amqp://admin:Password...@10.99.5.246/%2f')
    params = pika.URLParameters(url)
    connection = pika.BlockingConnection(params)
    channel = connection.channel()

    channel.queue_declare(queue='image_send3')

    channel.basic_consume(callback,
                          queue='image_send3',
                          no_ack=True)

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()
