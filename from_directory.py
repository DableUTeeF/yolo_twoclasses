from __future__ import print_function
import cv2
import os
from frontend import YOLO, TimeDistributedYOLO
import json
from utils import draw_boxes
from PIL import Image
from preprocessing import parse_annotation
import numpy as np
import argparse


def _main_():
    config_path = 'config/config_4.json'
    models = 'yolo'
    with open(config_path) as config_buffer:
        config = json.loads(config_buffer.read())
    if models == 'yolo':
        yolo = YOLO(architecture=config['model']['architecture'],
                    input_size=config['model']['input_size'],
                    types=config['model']['types'],
                    brand=config['model']['brand'],
                    max_box_per_image=config['model']['max_box_per_image'],
                    anchors=config['model']['anchors'], trainable=True,
                    debug=config['train']['debug'])
    else:
        yolo = TimeDistributedYOLO(architecture=config['model']['architecture'],
                                   input_size=config['model']['input_size'],
                                   types=config['model']['types'],
                                   brand=config['model']['brand'],
                                   max_box_per_image=config['model']['max_box_per_image'],
                                   anchors=config['model']['anchors'], trainable=True,
                                   debug=config['train']['debug'])

    ###############################
    #   Load the pretrained weights (if any)
    ###############################

    if os.path.exists(config['train']['pretrained_weights']):
        print("Loading pre-trained weights in", config['train']['pretrained_weights'])
        yolo.load_weights(config['train']['pretrained_weights'])
        #########################

    ''' ----------------------Evaluate------------------- '''
    if 0:
        train_imgs, [_, _] = parse_annotation(config['train']['train_annot_folder'],
                                              config['train']['train_image_folder'],
                                              config['model']['types'])
        train_imgs = train_imgs[1000:]

        results = yolo.evaluate(test_imgs=train_imgs,
                                batch_size=config['train']['batch_size'],
                                warmup_epochs=config['train']['warmup_epochs'],
                                test_times=config['train']['train_times'],
                                object_scale=config['train']['object_scale'],
                                no_object_scale=config['train']['no_object_scale'],
                                coord_scale=config['train']['coord_scale'],
                                class_scale=config['train']['class_scale'], debug=False)
        print(results)
    path = '/media/palm/Unimportant/phuket_image/12March2018/'
    # path = '/opt/work/car/car_yolo/car-Phuket-regioncrop/'
    counter = 0
    for filename in os.listdir(path):
        image = cv2.imread(path + filename)

        image = cv2.resize(image, (config['model']['input_size'][0],
                                   config['model']['input_size'][1]))
        boxes, out = yolo.predict(image, confidence_threshold=0.0, confidence_comfirm=False)
        image = draw_boxes(image, boxes, config['model']['types'], config['model']['brand'])
        im = Image.fromarray(image)
        b, g, r = im.split()
        im = Image.merge("RGB", (r, g, b))
        im.show()
        counter += 1
        if counter >= 10:
            break


if __name__ == '__main__':
    _main_()
