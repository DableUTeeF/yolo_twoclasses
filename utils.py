from __future__ import print_function, division
import numpy as np
import cv2


class BoundBox:
    def __init__(self, x, y, w, h, confidence=None, types=[], brand=[]):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.confident = confidence
        self.brand = brand
        self.types = types
        self.type_label = -1
        self.brand_label = -1
        self.score = -1
        self.brand_score = -1

    def get_score(self):
        if self.score == -1:
            self.score = self.types[self.get_type_label()]
        return self.score

    def get_type_score(self):
        return self.get_score()

    def get_brand_score(self):
        if self.brand_score == -1:
            self.brand_score = self.brand[self.get_brand_label()]
        return self.brand_score

    def get_type_label(self):
        if self.type_label == -1:
            self.type_label = np.argmax(self.types)

        return self.type_label

    def get_brand_label(self):
        if self.brand_label == -1:
            self.brand_label = np.argmax(self.brand)

        return self.brand_label


def bbox_iou(box1, box2):
    x1_min = box1.x - box1.w / 2
    x1_max = box1.x + box1.w / 2
    y1_min = box1.y - box1.h / 2
    y1_max = box1.y + box1.h / 2

    x2_min = box2.x - box2.w / 2
    x2_max = box2.x + box2.w / 2
    y2_min = box2.y - box2.h / 2
    y2_max = box2.y + box2.h / 2

    intersect_w = interval_overlap([x1_min, x1_max], [x2_min, x2_max])
    intersect_h = interval_overlap([y1_min, y1_max], [y2_min, y2_max])

    intersect = intersect_w * intersect_h

    union = box1.w * box1.h + box2.w * box2.h - intersect

    return float(intersect) / union


def interval_overlap(interval_a, interval_b):
    x1, x2 = interval_a
    x3, x4 = interval_b

    if x3 < x1:
        if x4 < x1:
            return 0
        else:
            return min(x2, x4) - x1
    else:
        if x2 < x3:
            return 0
        else:
            return min(x2, x4) - x3


def sigmoid(x):
    return 1. / (1. + np.exp(-x))


def softmax(x, axis=-1, t=-100.):
    x = x - np.max(x)

    if np.min(x) < t:
        x = x / np.min(x) * t

    e_x = np.exp(x)

    return e_x / e_x.sum(axis, keepdims=True)


def draw_boxes(image, boxes, types, brand):
    for box in boxes:
        xmin = max(int((box.x - box.w / 2) * image.shape[1]), 0)
        xmax = max(int((box.x + box.w / 2) * image.shape[1]), 0)
        ymin = max(int((box.y - box.h / 2) * image.shape[0]), 0)
        ymax = max(int((box.y + box.h / 2) * image.shape[0]), 0)

        cv2.rectangle(image, (xmin, ymin), (xmax, ymax), (0, 255, 0), 1)
        cv2.putText(image,
                    types[box.get_type_label()] + ' ' + str(box.get_type_score()),
                    (xmin + 10, ymin + 100),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1e-3 * image.shape[0],
                    (250, 255, 100), 1)
        cv2.putText(image,
                    'confidence: ' + str(box.confident),
                    (xmin + 10, ymin + 140),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1e-3 * image.shape[0],
                    (0, 255, 200), 1)
        try:
            cv2.putText(image,
                        brand[box.get_brand_label()] + ' ' + str(box.get_brand_score()),
                        (xmin + 10, ymin + 120),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        1e-3 * image.shape[0],
                        (100, 100, 255), 1)
        except ValueError:
            pass

    return image


def return_label(image, boxes, types, brand, id, loc):
    box_dict = {'id': id, 'location': loc, 'boxes': [], 'type': ''}
    for box in boxes:
        srttype = np.argsort(box.types)
        xmin = max(int((box.x - box.w / 2) * image.shape[1]), 0)
        xmax = max(int((box.x + box.w / 2) * image.shape[1]), 0)
        ymin = max(int((box.y - box.h / 2) * image.shape[0]), 0)
        ymax = max(int((box.y + box.h / 2) * image.shape[0]), 0)
        box_dict['boxes'] = [xmin, ymin, xmax, ymax]
        box_dict['type'] = {types[srttype[-1]]: str(box.types[srttype[-1]]),}
        try:
            srtbrand = np.argsort(box.brand)
            box_dict['brand'] = {brand[srtbrand[-1]]: str(box.brand[srtbrand[-1]]),}
        except ValueError:
            pass
    return box_dict
