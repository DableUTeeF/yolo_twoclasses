r"""
    MNetsV1: 36 img/s with my gtx950m
    MnetsV2: 30 img/s with my gtx950m
"""
from __future__ import print_function
import cv2
import os
from frontend import YOLO, TimeDistributedYOLO
import json
from utils import draw_boxes
from PIL import Image
from preprocessing import parse_annotation
import numpy as np
import argparse
import time


def _main_():
    config_path = 'config/config_4.json'
    models = 'yolo'
    with open(config_path) as config_buffer:
        config = json.loads(config_buffer.read())
    if models == 'yolo':
        yolo = YOLO(architecture=config['model']['architecture'],
                    input_size=config['model']['input_size'],
                    types=config['model']['types'],
                    brand=config['model']['brand'],
                    max_box_per_image=config['model']['max_box_per_image'],
                    anchors=config['model']['anchors'], trainable=True,
                    debug=config['train']['debug'])
    else:
        yolo = TimeDistributedYOLO(architecture=config['model']['architecture'],
                                   input_size=config['model']['input_size'],
                                   types=config['model']['types'],
                                   brand=config['model']['brand'],
                                   max_box_per_image=config['model']['max_box_per_image'],
                                   anchors=config['model']['anchors'], trainable=True,
                                   debug=config['train']['debug'])

    ###############################
    #   Load the pretrained weights (if any)
    ###############################

    if os.path.exists(config['train']['pretrained_weights']):
        print("Loading pre-trained weights in", config['train']['pretrained_weights'])
        yolo.load_weights(config['train']['pretrained_weights'])
        #########################
    init = 0
    path = '/home/palm/Pictures/'
    # path = '/opt/work/car/car_yolo/car-Phuket-regioncrop/'
    image = cv2.imread(path + 'asuna.png')
    image = cv2.resize(image, (config['model']['input_size'][0],
                               config['model']['input_size'][1]))
    for counter in range(2000):

        boxes, out = yolo.predict(image, confidence_threshold=0.0, confidence_comfirm=False)
        image = draw_boxes(image, boxes, config['model']['types'], config['model']['brand'])
        counter += 1
        if counter >= 100:
            # startime = time.time()
            # if time.time() - startime > 10:
            if init == 0:
                init = time.time()
            print(f'{(counter-100) / (time.time() - init)} imgs/s')

        if counter >= 2000:
            break


if __name__ == '__main__':
    _main_()
