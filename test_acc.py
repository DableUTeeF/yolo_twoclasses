from __future__ import print_function
import cv2
import os
from frontend import YOLO, TimeDistributedYOLO
import json
from utils import draw_boxes
from PIL import Image
from preprocessing import parse_annotation
import numpy as np
import argparse
import xml.etree.ElementTree as ET


def _main_():
    config_path = 'config/config_4.json'
    models = 'yolo'
    with open(config_path) as config_buffer:
        config = json.loads(config_buffer.read())
    if models == 'yolo':
        yolo = YOLO(architecture=config['model']['architecture'],
                    input_size=config['model']['input_size'],
                    types=config['model']['types'],
                    brand=config['model']['brand'],
                    max_box_per_image=config['model']['max_box_per_image'],
                    anchors=config['model']['anchors'], trainable=True,
                    debug=config['train']['debug'])
    else:
        yolo = TimeDistributedYOLO(architecture=config['model']['architecture'],
                                   input_size=config['model']['input_size'],
                                   types=config['model']['types'],
                                   brand=config['model']['brand'],
                                   max_box_per_image=config['model']['max_box_per_image'],
                                   anchors=config['model']['anchors'], trainable=True,
                                   debug=config['train']['debug'])

    ###############################
    #   Load the pretrained weights (if any)
    ###############################

    if os.path.exists(config['train']['pretrained_weights']):
        print("Loading pre-trained weights in", config['train']['pretrained_weights'])
        yolo.load_weights(config['train']['pretrained_weights'])
        #########################

    # path = '/media/palm/Unimportant/phuket_image/12March2018/'
    path = '/opt/work/car/car_yolo/car-Phuket-regioncrop/'
    xmls = '/opt/work/car/car_yolo/xml5/'
    counter = 0
    brandacc = 0
    typesacc = 0
    a = sorted(os.listdir(path))
    # a = a[int(len(a)*0.8):]
    for filename in a:
        counter += 1
        image = cv2.imread(path + filename)

        image = cv2.resize(image, (config['model']['input_size'][0],
                                   config['model']['input_size'][1]))
        boxes, out = yolo.predict(image, confidence_threshold=0.0, confidence_comfirm=False)
        types, brands = predict_labels(image, boxes, config['model']['types'], config['model']['brand'])
        root = ET.parse(xmls+filename[:-9]+'.xml')  # .jpg0.jpg
        for b in root.iter('brand'):
            if brands == b.text:
                brandacc += 1
        for t in root.iter('name'):
            if types == t.text:
                typesacc += 1
        print('type: ', int((typesacc/counter)*100), 'brand: ', int((brandacc/counter)*100), end='\r')
    print('')


def predict_labels(image, boxes, types, brand):
    for box in boxes:
        xmin = max(int((box.x - box.w / 2) * image.shape[1]), 0)
        xmax = max(int((box.x + box.w / 2) * image.shape[1]), 0)
        ymin = max(int((box.y - box.h / 2) * image.shape[0]), 0)
        ymax = max(int((box.y + box.h / 2) * image.shape[0]), 0)

        cv2.rectangle(image, (xmin, ymin), (xmax, ymax), (0, 255, 0), 1)
        cv2.putText(image,
                    types[box.get_type_label()] + ' ' + str(box.get_type_score()),
                    (xmin + 10, ymin + 100),
                    cv2.FONT_HERSHEY_SIMPLEX,
                    1e-3 * image.shape[0],
                    (250, 255, 100), 1)
        return types[box.get_type_label()], brand[box.get_brand_label()]


if __name__ == '__main__':
    _main_()
