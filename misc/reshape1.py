from __future__ import print_function
import os

os.environ["CUDA_VISIBLE_DEVICES"] = "1"
os.environ["KERAS_BACKEND"] = "tensorflow"
from models import MobileNet
from keras.layers import GlobalAveragePooling2D, Reshape, Lambda, Conv2D, Activation, Input
from keras.models import Model

inp = Input((416, 224, 3))
model = MobileNet(input_tensor=inp,
                  weights=None,
                  classes=21)
feature = model.get_layer('conv_pw_13_relu').output
class_out_types = GlobalAveragePooling2D()(feature)
class_out_types = Reshape((1, 1, 1024))(class_out_types)
class_out_types = Conv2D(10, (1, 1), padding='same')(class_out_types)
class_out_types = Activation('softmax')(class_out_types)
class_out_types = Reshape((10,), name='types')(class_out_types)

class_out_brand = GlobalAveragePooling2D()(feature)
class_out_brand = Reshape((1, 1, 1024))(class_out_brand)
class_out_brand = Conv2D(11, (1, 1), padding='same')(class_out_brand)
class_out_brand = Activation('softmax')(class_out_brand)
class_out_brand = Reshape((11,), name='brand')(class_out_brand)

model = Model(model.input, [class_out_types, class_out_brand])
true_boxes = Input(shape=(1, 1, 1, 1, 4))
model.load_weights('../weights/calsify_2cls_1.h5')
output = model.get_layer('conv_pw_13_relu').output
output = Conv2D(5 * (4 + 1 + 10 + 11),
                (1, 1), strides=(1, 1),
                padding='same',
                name='conv_23',
                kernel_initializer='lecun_normal')(output)
output = Reshape((13, 7, 5, 4 + 1 + 10 + 11))(output)
output = Lambda(lambda args: args[0])([output, true_boxes])

model = Model([model.input, true_boxes], output)
model.save_weights('../weights/mobilenet_classify_2cls_2.h5')
