import cv2
import os
from frontend import YOLO
import json
from utils import draw_boxes
from PIL import Image
from datetime import datetime
import time


def _main_():
    config_path = 'config/config.json'

    with open(config_path) as config_buffer:
        config = json.loads(config_buffer.read())

    yolo = YOLO(architecture=config['model']['architecture'],
                input_size=config['model']['input_size'],
                types=config['model']['types'],
                brand=config['model']['brand'],
                max_box_per_image=config['model']['max_box_per_image'],
                anchors=config['model']['anchors'], trainable=True,
                debug=config['train']['debug'])

    ###############################
    #   Load the pretrained weights (if any)
    ###############################

    if os.path.exists(config['train']['pretrained_weights']):
        print("Loading pre-trained weights in", config['train']['pretrained_weights'])
        yolo.load_weights(config['train']['pretrained_weights'])
        #########################

        ###############################
        #   Start the training process
        ###############################
    path = '/home/palm/Pictures/'
    counter = 0
    filename = 'asuna.png'
    image = cv2.imread(path+filename)

    image = cv2.resize(image, (224, 416))
    hour = datetime.now().hour
    minute = datetime.now().minute
    second = datetime.now().second
    ms = datetime.now().microsecond
    j = 0
    sums = 0
    while True:
        t = time.time()

        boxes = yolo.predict(image, confidence_threshold=0.3, verbose=0)
        if j > 0:
            sums += time.time() - t
        print(sums/(j+1))
        j += 1
    image = draw_boxes(image, boxes, config['model']['types'], config['model']['brand'])
    im = Image.fromarray(image)
    b, g, r = im.split()
    im = Image.merge("RGB", (r, g, b))
    im.show()


if __name__ == '__main__':
    _main_()
