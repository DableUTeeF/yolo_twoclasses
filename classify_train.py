from __future__ import print_function
import os

os.environ["CUDA_VISIBLE_DEVICES"] = "1"
os.environ["KERAS_BACKEND"] = "tensorflow"
from models import MobileNet
from keras.layers import GlobalAveragePooling2D, Reshape, Dropout, Conv2D, Activation, Input
from keras.models import Model
from keras.optimizers import SGD
from preprocessing import ClassifyBatchGenerator
from keras.callbacks import ReduceLROnPlateau, ModelCheckpoint
from preprocessing import parse_annotation
import json
import numpy as np
from keras import backend as K


def train(generator, epochs, valid_gen):
    inp = Input((284, 160, 3))
    model = MobileNet(input_tensor=inp,
                      weights=None,
                      classes=21)
    feature = model.get_layer('conv_pw_13_relu').output
    class_out_types = GlobalAveragePooling2D()(feature)
    class_out_types = Reshape((1, 1, 1024))(class_out_types)
    class_out_types = Conv2D(10, (1, 1), padding='same')(class_out_types)
    class_out_types = Activation('softmax')(class_out_types)
    class_out_types = Reshape((10,), name='types')(class_out_types)

    class_out_brand = GlobalAveragePooling2D()(feature)
    class_out_brand = Reshape((1, 1, 1024))(class_out_brand)
    class_out_brand = Conv2D(11, (1, 1), padding='same')(class_out_brand)
    class_out_brand = Activation('softmax')(class_out_brand)
    class_out_brand = Reshape((11,), name='brand')(class_out_brand)

    model = Model(model.input, [class_out_types, class_out_brand])

    model.load_weights('weights/mobilenet_classify_2cls.h5')
    opt = SGD(lr=0.01, decay=1e-6, momentum=0.9)
    model.compile(optimizer=opt, loss=['categorical_crossentropy', custom_loss], metrics=['acc'])
    checkpoint = ModelCheckpoint('weights/calsify_2cls_2.h5',
                                 monitor='val_brand_acc',
                                 save_weights_only=True, verbose=1, save_best_only=True)
    reducelr = ReduceLROnPlateau(monitor='val_brand_acc', factor=0.5, patience=5)
    model.fit_generator(generator,
                        callbacks=[checkpoint, reducelr],
                        epochs=epochs,
                        validation_data=valid_gen)


def custom_loss(y_true, y_pred):
    weights = [1 - 96. / 4200, 1 - 126. / 4200, 1 - 56. / 4200, 1 - 513. / 4200,
               1 - 511. / 4200, 1 - 143. / 4200, 1 - 251. / 4200, 1 - 155. / 4200,
               1 - 2012. / 4200, 1 - 64. / 4200, 1 - 273. / 4200]

    # scale predictions so that the class probas of each sample sum to 1
    y_pred /= K.sum(y_pred, axis=-1, keepdims=True)
    # clip to prevent NaN's and Inf's
    y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())
    # calc
    loss = y_true * K.log(y_pred) * weights
    loss = -K.sum(loss, -1)
    return loss


def normalize(image):
    image = image / 255.
    image = image - 0.5
    image = image * 2.

    return image


if __name__ == '__main__':
    config_path = 'config/config_4.json'
    with open(config_path) as config_buffer:
        config = json.loads(config_buffer.read())

    train_imgs, [train_types, train_brand] = parse_annotation(config['train']['train_annot_folder'],
                                                              config['train']['train_image_folder'],
                                                              config['model']['types'])

    if os.path.exists(config['valid']['valid_annot_folder']):
        valid_imgs, [valid_types, valid_brand] = parse_annotation(config['valid']['valid_annot_folder'],
                                                                  config['valid']['valid_image_folder'],
                                                                  config['model']['types'])
    else:
        train_valid_split = int(0.8 * len(train_imgs))
        np.random.shuffle(train_imgs)

        valid_imgs = train_imgs[train_valid_split:]
        train_imgs = train_imgs[:train_valid_split]

    overlap_labels = set(config['model']['brand']).intersection(set(train_brand.keys()))

    print('Seen labels:\t', train_brand)
    print('Given labels:\t', config['model']['brand'])
    print('Overlap labels:\t', overlap_labels)

    if len(overlap_labels) < len(config['model']['brand']):
        raise ValueError('Some labels have no images! Please revise the list of labels in the config.json file!')
    generator_config = {
        'IMAGE_H': 284,
        'IMAGE_W': 160,
        'TYPES': config['model']['types'],  # 'LABELS': self.labels,
        'N_TYPES': len(config['model']['types']),  # 'CLASS': len(self.labels),
        'BRAND': config['model']['brand'],
        'N_BRAND': len(config['model']['brand']),
        'BATCH_SIZE': 32,
    }

    train_gene = ClassifyBatchGenerator(train_imgs,
                                        generator_config,
                                        norm=normalize)
    valid_gene = ClassifyBatchGenerator(valid_imgs,
                                        generator_config,
                                        norm=normalize)

    train(train_gene, epochs=80, valid_gen=valid_gene)
