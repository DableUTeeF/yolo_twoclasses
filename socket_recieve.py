# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
import os
from frontend import YOLO, TimeDistributedYOLO
import json
from utils import draw_boxes
from PIL import Image
from datetime import datetime
import socket
from multiprocessing import Process, Pipe
import base64


class TrieNode(object):
    """
    #Our trie node implementation. Very basic. but does the job
    """

    def __init__(self, parent, char):
        self.char = char
        self.parent = parent
        self.children = []
        # Is it the last character of the word.`
        self.word_finished = -1
        # How many times this character appeared in the addition process
        self.counter = 1


def add(root, word):
    """
    Adding a word in the trie structure
    """
    global node_id
    #    saving = root
    node = root
    for char in word:
        found_in_child = False
        # Search for the character in the children of the present `node`
        for child in node.children:
            if child.char == char:
                # We found it, increase the counter by 1 to keep track that another
                # word has it as well
                child.counter += 1
                # And point the node to the child that contains this char
                node = child
                found_in_child = True
                break
        # We did not find it so add a new chlid
        if not found_in_child:
            new_node = TrieNode(node, char)
            node.children.append(new_node)
            # And then point node to the new child
            node = new_node
    # Everything finished. Mark it as the end of a word.
    node.word_finished = node_id
    node_id += 1
    #    root = saving
    return root, node.word_finished


def find(root, prefix):
    """
    Check and return
      1. If the prefix exsists in any of the words we added so far
      2. If yes then how may words actually have the prefix
    """
    #    global root,node_id,last_index,last_update
    count = 0
    node = root
    # If the root node has no children, then return False.
    # Because it means we are trying to search in an empty trie
    if not root.children:
        return -1
    for char in prefix:
        char_not_found = True
        # Search through all the children of the present `node`
        for child in node.children:
            if child.char == char:
                # We found the char existing in the child.
                char_not_found = False
                # Assign node as the child containing the char and break
                node = child
                break
        # Return False anyway when we did not find a char.
        if char_not_found:
            return -1
    # Well, we are here means we have found the prefix. Return true to indicate that
    # And also the counter of the last node. This indicates how many words have this
    # prefix
    return node.word_finished


def searching(pipe):
    output_p, input_p = pipe
    comms_socket = socket.socket()
    comms_socket.bind(('127.0.0.1', 7000))
    comms_socket.listen(10)
    global _FINISH, node_id
    while True:
        try:
            connection, address = comms_socket.accept()
            data = connection.recv(1024).decode("UTF-8")
            if data:
                # Write data to pyaudio stream
                output_p.send(data)
                lpr_ans = output_p.recv()
                print("Find: " + data + " ---> Answer: " + str(lpr_ans))
                # lpr_ans = find(root,data)
                # print(lpr_ans)
                connection.send(str(lpr_ans).encode("UTF-8"))
                connection.shutdown()
                connection.close()
            if _FINISH:
                print("update has been terminate")
                break
        except KeyboardInterrupt:
            print("trie server terminated!!")
            _FINISH = True
            break
        except:
            pass


def updating(pipe):
    output_p, input_p = pipe
    global root, last_index, node_id, _FINISH, last_update
    while True:
        try:
            """
               Classify here 
            """
            if input_p.poll():
                imarray = base64.b64decode(input_p.recv())
                print(imarray[:20])
        except KeyboardInterrupt:
            print("KeyboardInterrupt")
            _FINISH = True
            break


def _main_(image):
    config_path = 'config/config.json'
    models = 'yolo'
    with open(config_path) as config_buffer:
        config = json.loads(config_buffer.read())
    if models == 'yolo':
        yolo = YOLO(architecture=config['model']['architecture'],
                    input_size=config['model']['input_size'],
                    types=config['model']['types'],
                    brand=config['model']['brand'],
                    max_box_per_image=config['model']['max_box_per_image'],
                    anchors=config['model']['anchors'], trainable=True,
                    debug=config['train']['debug'])
    else:
        yolo = TimeDistributedYOLO(architecture=config['model']['architecture'],
                                   input_size=config['model']['input_size'],
                                   types=config['model']['types'],
                                   brand=config['model']['brand'],
                                   max_box_per_image=config['model']['max_box_per_image'],
                                   anchors=config['model']['anchors'], trainable=True,
                                   debug=config['train']['debug'])

    if os.path.exists(config['train']['pretrained_weights']):
        print("Loading pre-trained weights in", config['train']['pretrained_weights'])
        yolo.load_weights(config['train']['pretrained_weights'])
        #########################

    boxes, out = yolo.predict(image, confidence_threshold=0.0, confidence_comfirm=False)
    image = draw_boxes(image, boxes, config['model']['types'], config['model']['brand'])
    im = Image.fromarray(image)
    b, g, r = im.split()
    im = Image.merge("RGB", (r, g, b))
    return im


if __name__ == "__main__":
    output_p, input_p = Pipe()
    last_update = None
    _FINISH = False
    try:
        raise Exception
    except:
        node_id = 1
        root = TrieNode('', '*')
        last_index = 0
        print("No previous data found")
    if last_update is None:
        last_update = datetime.today()
    #
    #
    #
    #
    #
    p1 = Process(target=updating, args=((output_p, input_p),))
    # p2 = Process(target=searching, args=((output_p, input_p),))
    p1.start()
    # p2.start()

    p1.join()
    # p2.join()

