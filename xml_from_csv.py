# -*- coding: utf-8 -*-
from __future__ import print_function
import os

import xml.etree.cElementTree as ET

if __name__ == '__main__':
    path = '/media/palm/Unimportant/phuket_image/12March2018/'
    label = open('misc/phuket_car_day2.csv', 'r').readlines()[1:]
    labels = [elm.split(',')[1] for elm in label]
    name = [elm.split(',')[0] for elm in label]
    bbx = [elm.split(',')[2:] for elm in label]
    save_path = 'car-Phuket-regioncrop/'
    l = {'รถกระบะมีหลังคา': 'Roofed pickup',
         'รถกระบะมีหลังคาสูง': 'High-roof pickup',
         'รถกระบะไม่มีหลังคา': 'Pickup',
         'รถจักรยานยนต์': 'Motorcycle',
         'รถตู้': 'Van',
         'รถนั่งสองตอน': '2-seat car',
         'รถบรรทุก': 'Truck',
         'รถบัส': 'Bus',
         'รถเก๋ง': 'Sedan',
         'รถเก๋งแวน': 'Van-Sedan/SUV',
         }

    for filename in name:
        idx_on_list = name.index(filename)  # find index of class and name on created list
        _pth = '/opt/work/car/12March2018/' + save_path
        f_root = ET.Element('annotation')
        ET.SubElement(f_root, 'filename').text = filename
        ET.SubElement(f_root, 'path').text = _pth + filename
        f_size = ET.SubElement(f_root, 'size')
        ET.SubElement(f_size, 'width').text = '576'
        ET.SubElement(f_size, 'height').text = '1024'
        f_object = ET.SubElement(f_root, 'object')
        ET.SubElement(f_object, 'name').text = l[labels[idx_on_list]]
        f_bndbx = ET.SubElement(f_object, 'bndbox')
        ET.SubElement(f_bndbx, 'xmin').text = str(bbx[idx_on_list][0])
        ET.SubElement(f_bndbx, 'ymin').text = str(bbx[idx_on_list][1])
        ET.SubElement(f_bndbx, 'xmax').text = str(bbx[idx_on_list][2])
        ET.SubElement(f_bndbx, 'ymax').text = str(bbx[idx_on_list][3])

        tree = ET.ElementTree(f_root)
        tree.write('/media/palm/Unimportant/phuket_image/xml/' + filename[:-4] + '.xml')
